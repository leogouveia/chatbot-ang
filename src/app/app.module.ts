import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatHomeModule } from './containers/chat-home/chat-home.module';
import { ChatInputComponent } from './components/chat-input/chat-input.component';
import { ChatMessageComponent } from './components/chat-message/chat-message.component';
import { ArrowComponent } from './components/chat-message/arrow/arrow.component';
import { AvatarComponent } from './components/chat-message/avatar/avatar.component';
import { MessageComponent } from './components/chat-message/message/message.component';

@NgModule({
  declarations: [AppComponent, ChatInputComponent, ChatMessageComponent, ArrowComponent, AvatarComponent, MessageComponent],
  imports: [BrowserModule, AppRoutingModule, ChatHomeModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
