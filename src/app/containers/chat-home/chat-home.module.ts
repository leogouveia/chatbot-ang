import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatHomeComponent } from './chat-home.component';
import { ChatContainerModule } from 'src/app/components/chat-container/chat-container.module';

@NgModule({
  declarations: [ChatHomeComponent],
  imports: [CommonModule, ChatContainerModule],
})
export class ChatHomeModule {}
