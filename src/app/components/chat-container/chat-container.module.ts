import { NgModule } from '@angular/core';
import { ChatContainerComponent } from './chat-container.component';
import { CommonModule } from '@angular/common';
import { NgIconsModule } from '@ng-icons/core';
import { bootstrapSendFill } from '@ng-icons/bootstrap-icons';

@NgModule({
  imports: [CommonModule, NgIconsModule.withIcons({ bootstrapSendFill })],
  exports: [ChatContainerComponent],
  declarations: [ChatContainerComponent],
  providers: [],
})
export class ChatContainerModule {}
