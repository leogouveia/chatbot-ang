import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-arrow',
  templateUrl: './arrow.component.html',
  styleUrls: ['./arrow.component.css'],
})
export class ArrowComponent {
  @Input() direction?: 'reverse';
}
