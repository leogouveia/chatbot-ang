import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArrowComponent } from './arrow/arrow.component';
import { AvatarComponent } from './avatar/avatar.component';
import { MessageComponent } from './message/message.component';
import { ChatMessageComponent } from './chat-message.component';

@NgModule({
  declarations: [
    ArrowComponent,
    AvatarComponent,
    MessageComponent,
    ChatMessageComponent,
  ],
  imports: [CommonModule],
  exports: [ChatMessageComponent],
})
export class ChatMessageModule {}
